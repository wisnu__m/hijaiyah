package com.pausbiru.hijaiyah

import android.animation.Animator
import android.animation.AnimatorInflater
import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.app.Activity
import android.util.DisplayMetrics
import android.view.View
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdView
import com.google.android.gms.ads.MobileAds
import kotlinx.android.synthetic.main.activity_learn.*

abstract class BaseActivity : Activity() {

    companion object {
        const val DEFAULT_ANIMATION_DURATION = 500L
        const val QUICK_ANIMATION_DURATION = 300L
        const val QUICKER_ANIMATION_DURATION = 150L
    }

    var screenHeight = 0f
    var screenWidth = 0f

    fun initAds(adView: AdView) {
        MobileAds.initialize(this)
//        val adRequest = AdRequest.Builder()
//                .addTestDevice(getString(R.string.test_device))
//                .tagForChildDirectedTreatment(true)
//                .build()
        val adRequest = AdRequest.Builder().build()
        adView.loadAd(adRequest)
    }

    protected fun animatePopping(v: View) {
        val animator = AnimatorInflater.loadAnimator(this, R.animator.popping) as AnimatorSet
        animator.setTarget(v)
        animator.start()
    }

    protected fun animatePoppingWithAction(button: View, action: () -> Unit) {
        val animator = AnimatorInflater.loadAnimator(this, R.animator.popping) as AnimatorSet
        animator.setTarget(button)

        animator.addListener(object : Animator.AnimatorListener{
            override fun onAnimationEnd(p0: Animator?) {
                action()
            }

            override fun onAnimationStart(p0: Animator?) { }
            override fun onAnimationCancel(p0: Animator?) { }
            override fun onAnimationRepeat(p0: Animator?) { }
        })

        animator.start()
    }

    protected fun animateSqueeze(view: View) {
        val xAnimator = ObjectAnimator.ofFloat(view, "scaleX", 1.0f, 0.2f)
                .apply {
                    duration = QUICKER_ANIMATION_DURATION
                    repeatCount = 1
                    repeatMode = ObjectAnimator.REVERSE
                }

        val yAnimator = ObjectAnimator.ofFloat(view, "scaleY", 1.0f, 0.2f)
                .apply {
                    duration = QUICKER_ANIMATION_DURATION
                    repeatCount = 1
                    repeatMode = ObjectAnimator.REVERSE
                }

        AnimatorSet().apply {
            playTogether(xAnimator, yAnimator)
            start()
        }
    }

    protected fun animateShake(view: View) {
        val rotate1Animator = ObjectAnimator.ofFloat(view, "rotation", 0f, -30f)
                .apply {
                    duration = QUICKER_ANIMATION_DURATION
                }
        val rotate2Animator = ObjectAnimator.ofFloat(view, "rotation", -30f, 60f)
                .apply {
                    duration = QUICK_ANIMATION_DURATION
                }
        val rotate3Animator = ObjectAnimator.ofFloat(view, "rotation", 60f, 0f)
                .apply {
                    duration = QUICKER_ANIMATION_DURATION
                }
        AnimatorSet().apply {
            playSequentially(rotate1Animator, rotate2Animator, rotate3Animator)
            start()
        }
    }

    override fun onResume() {
        super.onResume()

        val displayMetrics = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(displayMetrics)
        screenHeight = displayMetrics.heightPixels.toFloat()
        screenWidth = displayMetrics.widthPixels.toFloat()
    }
}
