package com.pausbiru.hijaiyah

object Constant {

    const val DEFAULT_QUIZ_NUMBER = 10
    const val DEFAULT_ANSWER_NUMBER = 4

    val contentArray = arrayOf(
            R.drawable.ctn_huruf_1, R.drawable.ctn_huruf_2,R.drawable.ctn_huruf_3,
            R.drawable.ctn_huruf_4, R.drawable.ctn_huruf_5,R.drawable.ctn_huruf_6,
            R.drawable.ctn_huruf_7, R.drawable.ctn_huruf_8,R.drawable.ctn_huruf_9,
            R.drawable.ctn_huruf_10, R.drawable.ctn_huruf_11,R.drawable.ctn_huruf_12,
            R.drawable.ctn_huruf_13, R.drawable.ctn_huruf_14,R.drawable.ctn_huruf_15,
            R.drawable.ctn_huruf_16, R.drawable.ctn_huruf_17,R.drawable.ctn_huruf_18,
            R.drawable.ctn_huruf_19, R.drawable.ctn_huruf_20,R.drawable.ctn_huruf_21,
            R.drawable.ctn_huruf_22, R.drawable.ctn_huruf_23,R.drawable.ctn_huruf_24,
            R.drawable.ctn_huruf_25, R.drawable.ctn_huruf_26,R.drawable.ctn_huruf_27,
            R.drawable.ctn_huruf_28, R.drawable.ctn_huruf_29,R.drawable.ctn_huruf_30)

    val arabicStringArray = arrayOf(
            "ا", "ب", "ت", "ث", "ج", "ح", "خ", "د", "ذ", "ر",
            "ز", "س", "ش", "ص", "ض", "ط", "ظ", "ع", "غ", "ف",
            "ق", "ك", "ل", "م", "ن", "و", "ه", "لا", "ء", "ي"
    )

    val indoStringArray = arrayOf(
            "alif", "ba", "ta", "tsa", "jim", "ha", "kho", "dal", "dzal", "ro",
            "za", "sin", "syin", "shod", "dhod", "tho", "zho", "ain", "ghoin", "fa",
            "qof", "kaf", "lam", "mim", "nun", "waw", "ha", "lam alif", "hamzah", "ya"
    )

    val contentColors = arrayOf(
            R.color.contentBlue, R.color.contentGreen, R.color.contentRed,
            R.color.contentOrange, R.color.contentLightBlue, R.color.contentPurple
    )

    // must be the same size as contentColors
    val labelBackground = arrayOf(
            R.drawable.label_1_blue, R.drawable.label_2_green, R.drawable.label_3_merah,
            R.drawable.label_4_orange, R.drawable.label_5_lightblue, R.drawable.label_6_purple
    )

    val labelArray = arrayOf(
            R.drawable.label_1, R.drawable.label_2,R.drawable.label_3,
            R.drawable.label_4, R.drawable.label_5,R.drawable.label_6,
            R.drawable.label_7, R.drawable.label_8,R.drawable.label_9,
            R.drawable.label_10, R.drawable.label_11,R.drawable.label_12,
            R.drawable.label_13, R.drawable.label_14,R.drawable.label_15,
            R.drawable.label_16, R.drawable.label_17,R.drawable.label_18,
            R.drawable.label_19, R.drawable.label_20,R.drawable.label_21,
            R.drawable.label_22, R.drawable.label_23,R.drawable.label_24,
            R.drawable.label_25, R.drawable.label_26,R.drawable.label_27,
            R.drawable.label_28, R.drawable.label_29,R.drawable.label_30)

    val quizArray = arrayOf(
            R.drawable.qz_huruf_1, R.drawable.qz_huruf_2,R.drawable.qz_huruf_3,
            R.drawable.qz_huruf_4, R.drawable.qz_huruf_5,R.drawable.qz_huruf_6,
            R.drawable.qz_huruf_7, R.drawable.qz_huruf_8,R.drawable.qz_huruf_9,
            R.drawable.qz_huruf_10, R.drawable.qz_huruf_11,R.drawable.qz_huruf_12,
            R.drawable.qz_huruf_13, R.drawable.qz_huruf_14,R.drawable.qz_huruf_15,
            R.drawable.qz_huruf_16, R.drawable.qz_huruf_17,R.drawable.qz_huruf_18,
            R.drawable.qz_huruf_19, R.drawable.qz_huruf_20,R.drawable.qz_huruf_21,
            R.drawable.qz_huruf_22, R.drawable.qz_huruf_23,R.drawable.qz_huruf_24,
            R.drawable.qz_huruf_25, R.drawable.qz_huruf_26,R.drawable.qz_huruf_27,
            R.drawable.qz_huruf_28, R.drawable.qz_huruf_29,R.drawable.qz_huruf_30)

    val soundArray = arrayOf(
            R.raw.kk_01_alif, R.raw.kk_02_ba, R.raw.kk_03_ta,
            R.raw.kk_04_tsa, R.raw.kk_05_jim, R.raw.kk_06_ha,
            R.raw.kk_07_kho, R.raw.kk_08_dal, R.raw.kk_09_dzal,
            R.raw.kk_10_ro, R.raw.kk_11_zain, R.raw.kk_12_sin,
            R.raw.kk_13_syin, R.raw.kk_14_shod, R.raw.kk_15_dhod,
            R.raw.kk_16_tho, R.raw.kk_17_zho, R.raw.kk_18_ain,
            R.raw.kk_19_ghoin, R.raw.kk_20_fa, R.raw.kk_21_qof,
            R.raw.kk_22_kaf, R.raw.kk_23_lam, R.raw.kk_24_mim,
            R.raw.kk_25_nun, R.raw.kk_26_wau, R.raw.kk_27_ha,
            R.raw.kk_28_lamalif, R.raw.kk_29_hamzah, R.raw.kk_30_ya
            //R.raw.sound_1, R.raw.sound_2,R.raw.sound_3,
            //R.raw.sound_4, R.raw.sound_5,R.raw.sound_6,
            //R.raw.sound_7, R.raw.sound_8,R.raw.sound_9,
            //R.raw.sound_10, R.raw.sound_11,R.raw.sound_12,
            //R.raw.sound_13, R.raw.sound_14,R.raw.sound_15,
            //R.raw.sound_16, R.raw.sound_17,R.raw.sound_18,
            //R.raw.sound_19, R.raw.sound_20,R.raw.sound_21,
            //R.raw.sound_22, R.raw.sound_23,R.raw.sound_24,
            //R.raw.sound_25, R.raw.sound_26,R.raw.sound_27,
            //R.raw.sound_28, R.raw.sound_29,R.raw.sound_30
    )

    /*fun getContent(idx: Int): Int { return contentArray[idx] }

    fun getLabel(idx: Int): Int { return labelArray[idx] }

    fun getQuiz(idx: Int): Int { return quizArray[idx] }

    fun getContentLength(): Int { return contentArray.size }*/
}