package com.pausbiru.hijaiyah

import android.animation.Animator
import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.media.MediaPlayer
import android.os.Bundle
import android.view.View

import com.google.android.gms.ads.AdView
import kotlinx.android.synthetic.main.activity_learn.*
import kotlin.random.Random

class LearnActivity : BaseActivity() {

    //private var maxIdx: Int = constant.getContentLength()
    private var currentIdx: Int = 0
    private var isSoundPlaying: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_learn)

        // set initial content and button listener
        button_left.setOnClickListener {
            animatePopping(it)
            animateContent(it)
        }
        button_right.setOnClickListener {
            animatePopping(it)
            animateContent(it)
        }
        btn_home_learn.setOnClickListener {
            animatePopping(it)
            isSoundPlaying = false
            finish()
        }

        initView()

        // Load AdMob
        val adView = findViewById<AdView>(R.id.adviewlearn)
        initAds(adView)
    }

    private fun initView() {
        currentIdx = 0
        button_left.visibility = View.INVISIBLE
        setContent(currentIdx)
        playSound(Constant.soundArray[currentIdx])
    }

    private fun animateContent(v: View) {
        var v1 = 0f
        var v2 = 0f

        if (v.id == R.id.button_right) {
            currentIdx += 1
            v1 = screenWidth
            v2 = -screenWidth
        } else {
            currentIdx -= 1
            v1 = -screenWidth
            v2 = screenWidth
        }

        val maxIdx = Constant.contentArray.size
        when (currentIdx) {
            0 -> button_left.visibility = View.INVISIBLE
            maxIdx-1 -> button_right.visibility = View.INVISIBLE
            else -> {
                button_left.visibility = View.VISIBLE
                button_right.visibility = View.VISIBLE
            }
        }

        val outAnimator = ObjectAnimator.ofFloat(content_learn_layout as View, "translationX", 0f, v2)
        outAnimator.addListener(object : Animator.AnimatorListener{
            override fun onAnimationEnd(p0: Animator?) {
                setContent(currentIdx)
            }

            override fun onAnimationStart(p0: Animator?) { }
            override fun onAnimationCancel(p0: Animator?) { }
            override fun onAnimationRepeat(p0: Animator?) { }
        })
        outAnimator.duration = QUICK_ANIMATION_DURATION

        val inAnimator = ObjectAnimator.ofFloat(content_learn_layout as View, "translationX", v1, 0f)
        inAnimator.addListener(object : Animator.AnimatorListener{
            override fun onAnimationEnd(p0: Animator?) {
                playSound(Constant.soundArray[currentIdx])
            }

            override fun onAnimationStart(p0: Animator?) { }
            override fun onAnimationCancel(p0: Animator?) { }
            override fun onAnimationRepeat(p0: Animator?) { }
        })
        inAnimator.duration = QUICK_ANIMATION_DURATION

        val animSet = AnimatorSet()
        animSet.playSequentially(outAnimator, inAnimator)
        animSet.start()
    }

    fun setContent(idx: Int) {
        //content_learn.setBackgroundResource(Constant.contentArray[idx])
        content_learn_text.text = Constant.arabicStringArray[idx]
        //content_label.setBackgroundResource(Constant.labelArray[idx])
        content_label_text.text = Constant.indoStringArray[idx]

        var randomColorIdx = Random.nextInt(0, Constant.contentColors.size-1)
        content_learn_text.setTextColor(resources.getColor(Constant.contentColors[randomColorIdx]))
        content_label.setBackgroundResource(Constant.labelBackground[randomColorIdx])
    }

    fun onContentClick(view: View) {
        animatePopping(view)
        playSound(Constant.soundArray[currentIdx])
    }

    private fun playSound(soundId: Int) {
        // play button sound
        val mpBtn = MediaPlayer.create(this, soundId)
        mpBtn.setOnCompletionListener { mediaPlayer ->
            mediaPlayer.release()
            isSoundPlaying = false
        }
        isSoundPlaying = true
        mpBtn.start()
    }
}
