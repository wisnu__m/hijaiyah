package com.pausbiru.hijaiyah

import android.animation.Animator
import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.media.MediaPlayer
import android.os.Handler
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import android.widget.Toast

import com.google.android.gms.ads.AdView
import kotlinx.android.synthetic.main.activity_game.*

import kotlin.random.Random

import java.util.ArrayList

class GameActivity : BaseActivity() {

    private var quizIdx: Int = 0
    private var currentQz: Int = 0
    private var isSoundPlaying: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_game)

        // Load AdMob
        val mAdView = findViewById<View>(R.id.adviewgame) as AdView
        initAds(mAdView)
    }

    /*override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_game, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val id = item.itemId

        return if (id == R.id.action_settings) {
            true
        } else super.onOptionsItemSelected(item)

    }*/

    override fun onStart() {
        super.onStart()
        // Start the quiz
        createQuestion()
    }

    // Set the question, one time at a time
    private fun createQuestion() {
        val maxIdx: Int = Constant.contentArray.size-1
        val maxAns: Int = Constant.DEFAULT_ANSWER_NUMBER-1

        // set the question
        quizIdx = Random.nextInt(0, maxIdx+1)
        val colorIdx = Random.nextInt(0, Constant.contentColors.size)
        //val question = findViewById<ImageView>(R.id.question)
        //question.setBackgroundResource(Constant.labelArray[quizIdx])
        question_label.setBackgroundResource(Constant.labelBackground[colorIdx])
        question_text.text = Constant.indoStringArray[quizIdx]
        val soundId = Constant.soundArray[quizIdx]

        // play the question
        isSoundPlaying = true
        val mpBtn = MediaPlayer.create(this, R.raw.quiz)
        mpBtn.setOnCompletionListener { mediaPlayer ->
            playSound(soundId)
            mediaPlayer.release()
            isSoundPlaying = false
        }
        mpBtn.start()

        // set the "right" answer
        val ansIdx = Random.nextInt(0, maxAns+1) //randInt(0, maxAns)
        createOptions(ansIdx, quizIdx, colorIdx,true)

        // generate the "wrong" answers
        val listNum = ArrayList<Int>()
        listNum.add(quizIdx)
        for (i in 0..maxAns) {
            if (i != ansIdx) {
                val otherAns = randIntUnique(0, maxIdx, listNum)
                createOptions(i, otherAns, colorIdx,false)
                listNum.add(otherAns)
            }
        }

    }

    // generate possible answers
    private fun createOptions(position: Int, contentIdx: Int, colorIdx: Int, status: Boolean) {
        when (position) {
            0 -> {
                //val answer1 = findViewById<ImageView>(R.id.answer1)
                answer1_result.setImageDrawable(null)
                //answer1.setBackgroundResource(Constant.quizArray[contentIdx])
                answer1_text.text = Constant.arabicStringArray[contentIdx]
                answer1_text.setTextColor(resources.getColor(Constant.contentColors[colorIdx]))
                answer1_layout.tag = status
            }
            1 -> {
                //val answer2 = findViewById<ImageView>(R.id.answer2)
                answer2_result.setImageDrawable(null)
                //answer2.setBackgroundResource(Constant.quizArray[contentIdx])
                answer2_text.text = Constant.arabicStringArray[contentIdx]
                answer2_text.setTextColor(resources.getColor(Constant.contentColors[colorIdx]))
                answer2_layout.tag = status
            }
            2 -> {
                //val answer3 = findViewById<ImageView>(R.id.answer3)
                answer3_result.setImageDrawable(null)
                //answer3.setBackgroundResource(Constant.quizArray[contentIdx])
                answer3_text.text = Constant.arabicStringArray[contentIdx]
                answer3_text.setTextColor(resources.getColor(Constant.contentColors[colorIdx]))
                answer3_layout.tag = status
            }
            3 -> {
                //val answer4 = findViewById<ImageView>(R.id.answer4)
                answer4_result.setImageDrawable(null)
                //answer4.setBackgroundResource(Constant.quizArray[contentIdx])
                answer4_text.text = Constant.arabicStringArray[contentIdx]
                answer4_text.setTextColor(resources.getColor(Constant.contentColors[colorIdx]))
                answer4_layout.tag = status
            }
        }

    }

    // check if answer is correct
    fun onAnswerClicked(v: View) {
        if (isSoundPlaying) return
        val maxQz: Int = Constant.DEFAULT_QUIZ_NUMBER
        //val answer = v as ImageView

        if (v.tag != null) {

            val ansSoundId = Random.nextInt(1,3) //randInt(1, 2)

            val resultImage = when (v.id) {
                R.id.answer1_layout -> answer1_result
                R.id.answer2_layout -> answer2_result
                R.id.answer3_layout -> answer3_result
                R.id.answer4_layout -> answer4_result
                else -> null
            }

            if (java.lang.Boolean.parseBoolean(v.tag.toString())) { // Correct Answer

                val correctAnswerSoundId =
                        if (ansSoundId == 1) R.raw.sound_correct_1
                        else R.raw.sound_correct_2

                if (currentQz == maxQz) {
                    currentQz = 0
                    playSoundWithAction(correctAnswerSoundId) { onBackPressed() }
                    onBackPressed()
                } else {
                    playSoundWithAction(correctAnswerSoundId) { animateSqueezeIn() }
                }

                resultImage?.setImageResource(R.drawable.qz_correct)

                // delay 700 millisecond
                /*Handler().postDelayed(Runnable {
                    // go back to main screen if it's already reach the max number of question
                    if (currentQz == maxQz) {
                        currentQz = 0
                        onBackPressed()
                        return@Runnable
                    }
                    createQuestion()
                }, 2000)*/

            } else { // WRONG answer

                if (ansSoundId == 1)
                    playSound(R.raw.sound_wrong_1)
                else
                    playSound(R.raw.sound_wrong_2)

                resultImage?.setImageResource(R.drawable.qz_wrong)

                Handler().postDelayed({ }, 2000)
            }
        }

        currentQz += 1
    }

    fun animateSqueezeIn() {
        val qXInAnimator = ObjectAnimator.ofFloat(content_question as View, "scaleX", 1.0f, 0.2f)
                .apply { duration = QUICKER_ANIMATION_DURATION }
        qXInAnimator.addListener(object : Animator.AnimatorListener{
            override fun onAnimationEnd(p0: Animator?) { createQuestion() }
            override fun onAnimationStart(p0: Animator?) { }
            override fun onAnimationCancel(p0: Animator?) { }
            override fun onAnimationRepeat(p0: Animator?) { }
        })

        val qYInAnimator = ObjectAnimator.ofFloat(content_question as View, "scaleY", 1.0f, 0.2f)
                .apply { duration = QUICKER_ANIMATION_DURATION }

        val a1XInAnimator = ObjectAnimator.ofFloat(answer1_layout as View, "scaleX", 1.0f, 0.2f)
                .apply { duration = QUICKER_ANIMATION_DURATION }

        val a1YInAnimator = ObjectAnimator.ofFloat(answer1_layout as View, "scaleY", 1.0f, 0.2f)
                .apply { duration = QUICKER_ANIMATION_DURATION }

        val a2XInAnimator = ObjectAnimator.ofFloat(answer2_layout as View, "scaleX", 1.0f, 0.2f)
                .apply { duration = QUICKER_ANIMATION_DURATION }

        val a2YInAnimator = ObjectAnimator.ofFloat(answer2_layout as View, "scaleY", 1.0f, 0.2f)
                .apply { duration = QUICKER_ANIMATION_DURATION }

        val a3XInAnimator = ObjectAnimator.ofFloat(answer3_layout as View, "scaleX", 1.0f, 0.2f)
                .apply { duration = QUICKER_ANIMATION_DURATION }

        val a3YInAnimator = ObjectAnimator.ofFloat(answer3_layout as View, "scaleY", 1.0f, 0.2f)
                .apply { duration = QUICKER_ANIMATION_DURATION }

        val a4XInAnimator = ObjectAnimator.ofFloat(answer4_layout as View, "scaleX", 1.0f, 0.2f)
                .apply { duration = QUICKER_ANIMATION_DURATION }

        val a4YInAnimator = ObjectAnimator.ofFloat(answer4_layout as View, "scaleY", 1.0f, 0.2f)
                .apply { duration = QUICKER_ANIMATION_DURATION }

        val qXOutAnimator = ObjectAnimator.ofFloat(content_question as View, "scaleX", 0.2f, 1.0f)
                .apply { duration = QUICKER_ANIMATION_DURATION }

        val qYOutAnimator = ObjectAnimator.ofFloat(content_question as View, "scaleY", 0.2f, 1.0f)
                .apply { duration = QUICKER_ANIMATION_DURATION }

        val a1XOutAnimator = ObjectAnimator.ofFloat(answer1_layout as View, "scaleX", 0.2f, 1.0f)
                .apply { duration = QUICKER_ANIMATION_DURATION }

        val a1YOutAnimator = ObjectAnimator.ofFloat(answer1_layout as View, "scaleY", 0.2f, 1.0f)
                .apply { duration = QUICKER_ANIMATION_DURATION }

        val a2XOutAnimator = ObjectAnimator.ofFloat(answer2_layout as View, "scaleX", 0.2f, 1.0f)
                .apply { duration = QUICKER_ANIMATION_DURATION }

        val a2YOutAnimator = ObjectAnimator.ofFloat(answer2_layout as View, "scaleY", 0.2f, 1.0f)
                .apply { duration = QUICKER_ANIMATION_DURATION }

        val a3XOutAnimator = ObjectAnimator.ofFloat(answer3_layout as View, "scaleX", 0.2f, 1.0f)
                .apply { duration = QUICKER_ANIMATION_DURATION }

        val a3YOutAnimator = ObjectAnimator.ofFloat(answer3_layout as View, "scaleY", 0.2f, 1.0f)
                .apply { duration = QUICKER_ANIMATION_DURATION }

        val a4XOutAnimator = ObjectAnimator.ofFloat(answer4_layout as View, "scaleX", 0.2f, 1.0f)
                .apply { duration = QUICKER_ANIMATION_DURATION }

        val a4YOutAnimator = ObjectAnimator.ofFloat(answer4_layout as View, "scaleY", 0.2f, 1.0f)
                .apply { duration = QUICKER_ANIMATION_DURATION }

        val inAnimatorSet = AnimatorSet().apply {
            playTogether(qXInAnimator, qYInAnimator, a1XInAnimator, a1YInAnimator, a2XInAnimator,
                    a2YInAnimator, a3XInAnimator, a3YInAnimator, a4XInAnimator, a4YInAnimator)
        }
        val outAnimatorSet = AnimatorSet().apply {
            playTogether(qXOutAnimator, qYOutAnimator, a1XOutAnimator, a1YOutAnimator, a2XOutAnimator,
                    a2YOutAnimator, a3XOutAnimator, a3YOutAnimator, a4XOutAnimator, a4YOutAnimator)
        }
        AnimatorSet().apply {
            playSequentially(inAnimatorSet, outAnimatorSet)
            start()
        }
    }

    // repeat the question when clicked
    fun onQuizClick(view: View) {
        animatePopping(view)
        playSound(Constant.soundArray[quizIdx])
    }

    // to generate unique number other than in othernum
    private fun randIntUnique(min: Int, max: Int, othernum: List<*>): Int {
        val bound = max - min + 1
        var randNum = Random.nextInt(min, bound)

        var isUnique = false
        while (!isUnique) {
            randNum = Random.nextInt(min, bound)
            isUnique = !othernum.contains(randNum)
        }

        return randNum
    }

    private fun playSound(soundId: Int) {
        // play button sound
        val mpBtn = MediaPlayer.create(this, soundId)
        mpBtn.setOnCompletionListener { mediaPlayer ->
            mediaPlayer.release()
            isSoundPlaying = false
        }
        mpBtn.start()
    }

    private fun playSoundWithAction(soundId: Int, action: () -> Unit) {
        // play button sound
        val mpBtn = MediaPlayer.create(this, soundId)
        mpBtn.setOnCompletionListener { mediaPlayer ->
            mediaPlayer.release()
            isSoundPlaying = false
            action()
        }
        mpBtn.start()
    }

    fun onHomeClick(view: View) {
        animatePopping(view)
        finish()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }
}
