package com.pausbiru.hijaiyah

import android.animation.Animator
import android.animation.AnimatorInflater
import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.ImageView

import com.google.android.gms.ads.AdView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val imgTitle = findViewById<ImageView>(R.id.title)
        animateTitle(imgTitle)
        imgTitle.setOnClickListener { animateShake(it) }


        val btnLearn = findViewById<ImageView>(R.id.btn_learn)
        btnLearn.setOnClickListener {
            val intent = Intent(this@MainActivity, LearnActivity::class.java)
            animatePoppingWithAction(it) { startActivity(intent) }
        }

        val btnPlay = findViewById<ImageView>(R.id.btn_play)
        btnPlay.setOnClickListener {
            val intent = Intent(this@MainActivity, GameActivity::class.java)
            animatePoppingWithAction(it) { startActivity(intent) }
        }

        img_muslim_girl.setOnClickListener {
            it.pivotY = screenHeight
            it.pivotX = 0f
            val rotateAnimator = ObjectAnimator.ofFloat(it, "rotation", 0f, -5f)
                    .apply {
                        duration = QUICK_ANIMATION_DURATION
                        repeatCount = 1
                        repeatMode = ObjectAnimator.REVERSE
                    }
            rotateAnimator.start()
        }

        img_muslim_boy.setOnClickListener {
            it.pivotY = screenHeight
            it.pivotX = screenWidth
            val rotateAnimator = ObjectAnimator.ofFloat(it, "rotation", 0f, 5f)
                    .apply {
                        duration = QUICK_ANIMATION_DURATION
                        repeatCount = 1
                        repeatMode = ObjectAnimator.REVERSE
                    }
            rotateAnimator.start()
        }

        // load AdMob
        val mAdView = findViewById<AdView>(R.id.adview)
        initAds(mAdView)
    }

    /*private fun animatePoppingWithAction(button: View, action: () -> Unit) {
        val animator = AnimatorInflater.loadAnimator(this, R.animator.popping) as AnimatorSet
        animator.setTarget(button)

        animator.addListener(object : Animator.AnimatorListener{
            override fun onAnimationEnd(p0: Animator?) {
                action()
            }

            override fun onAnimationStart(p0: Animator?) { }
            override fun onAnimationCancel(p0: Animator?) { }
            override fun onAnimationRepeat(p0: Animator?) { }
        })

        animator.start()
    }*/

    private fun animateTitle(title: View) {
        val animator = ObjectAnimator.ofFloat(title, "translationY", -screenHeight, 0f)
        animator.duration = DEFAULT_ANIMATION_DURATION
        animator.start()
    }

}
